//
//  Tools.h
//
//  Created by Do Trinh on 2019/08/11.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface Tools : NSObject

+ (NSArray *)getRange:(int)start end:(int)end suffix:(NSString *)suffix;

+ (NSString *)append:(id)first, ...;
@end
