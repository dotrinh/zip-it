//
//  Tools.m
//
//  Created by Do Trinh on 2019/08/11.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import "Tools.h"

@implementation Tools

+ (NSArray *)getRange:(int)start end:(int)stop suffix:(NSString *)suffix {
    NSMutableArray *lastData = [[NSMutableArray alloc] init];
    while (start <= stop) {
        int currentNum = start++;
        NSString *name = @(currentNum).stringValue;
        NSString *item = [NSString stringWithFormat:@"%@%@", name, suffix];
        [lastData addObject:item];
    }
    NSArray *itemArr = [lastData copy];
    return itemArr;
}

+ (NSString *)append:(id)first, ... {
    NSString *result = @"";
    id eachArg;
    va_list alist;
    if (first) {
        result = [result stringByAppendingString:first];
        va_start(alist, first);
        while ((eachArg = va_arg(alist, id)))
            result = [result stringByAppendingString:eachArg];
        va_end(alist);
    }
    return result;
}
@end
