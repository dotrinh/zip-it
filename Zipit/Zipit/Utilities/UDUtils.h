//
//  UDUtils.h
//  Tepra Lite
//
//  Created by Do Trinh on 2020/01/07.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UDUtils : NSObject

//BOOL
+ (void) setBool: (NSString *) name val:(BOOL) val;
+ (BOOL) getBool: (NSString *) name;

//STRING
+ (void) setStr: (NSString *) name val:(NSString *)val;
+ (NSString *) getStr: (NSString *) name;

//INT
+ (void) setInt: (NSString *) name val:(NSInteger)val;
+ (NSInteger) getInt: (NSString *) name;
+ (BOOL) isExisted: (NSString *) name;
+ (void) removeUD: (NSString *) name;
@end
