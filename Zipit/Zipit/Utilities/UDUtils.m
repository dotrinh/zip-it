//
//  UDUtils.m
//  Tepra Lite
//
//  Created by Do Trinh on 2020/01/07.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import "UDUtils.h"

@implementation UDUtils
+ (void) setBool: (NSString *) name val:(BOOL)val{
    [[NSUserDefaults standardUserDefaults] setBool:val forKey:name];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"viewlog-------: %@", name);
}

+ (BOOL) getBool: (NSString *) name{
    return [[NSUserDefaults standardUserDefaults] boolForKey:name];
}

+ (void) setStr: (NSString *) name val:(NSString *)val{
    [[NSUserDefaults standardUserDefaults] setObject:val forKey:name];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"viewlog-------: %@ %@", name, val);
}

+ (NSString *) getStr: (NSString *) name{
    return [[NSUserDefaults standardUserDefaults] stringForKey:name];
}

+ (void) setInt: (NSString *) name val:(NSInteger)val{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setInteger:val forKey:name];
    [userDefault synchronize];
}

+ (NSInteger) getInt: (NSString *) name{
    return [[NSUserDefaults standardUserDefaults] integerForKey:name];
}
+ (BOOL) isExisted: (NSString *) name{
    return [[NSUserDefaults standardUserDefaults] objectForKey:name]!=nil;
}
+ (void) removeUD: (NSString *) name{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:name];
}
@end
