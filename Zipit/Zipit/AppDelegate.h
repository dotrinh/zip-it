//
//  AppDelegate.h
//  Zipit
//
//  Created by Do Trinh on 2020/02/05.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

