//
//  ViewController.m
//  Zipit
//
//  Created by Do Trinh on 2020/02/05.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import <SSZipArchive/SSZipArchive.h>
#import "ViewController.h"
#import "Tools.h"
#import "UDUtils.h"

@interface ViewController ()
@property(weak) IBOutlet NSTextField *passLBL;
@property(weak) IBOutlet NSTextField *statusZIP;
@property (weak) IBOutlet NSTextField *selectedPath;

@end

@implementation ViewController

NSString *DOTRINH;
NSString *outputPath;
NSString *finalFileZip;
NSString *inputPATH;

- (void)viewDidLoad {
    [super viewDidLoad];
    DOTRINH = NSUserName();
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    outputPath = [Tools append:@"/Users/", DOTRINH, @"/Downloads/aaaaaaaaaaaaaaaaaaaa/", nil];
    inputPATH = @"/Users/dotrinh/Downloads/data8/";
    if (![fileManager createDirectoryAtPath:outputPath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"Failed to create directory \"%@\". Error: %@", outputPath, error);
    }
    if ([UDUtils getStr:@"pass"].length >= 1) {
        [_passLBL setStringValue:[UDUtils getStr:@"pass"]];
    }
}

- (IBAction)startZIP:(id)sender {
    NSString *password = [_passLBL stringValue];
    BOOL isOK;
    finalFileZip = [Tools append:outputPath, [self getFileName], nil];
    if ([password length] < 1) {
        [UDUtils setStr:@"pass" val:@""];
        isOK = [SSZipArchive createZipFileAtPath:finalFileZip withContentsOfDirectory:inputPATH withPassword:nil];
    } else {
        [UDUtils setStr:@"pass" val:password];
        isOK = [SSZipArchive createZipFileAtPath:finalFileZip withContentsOfDirectory:inputPATH withPassword:password];
    }
    if (isOK) {
        _statusZIP.hidden = false;
        _statusZIP.textColor = [NSColor systemBlueColor];
    } else {
        _statusZIP.hidden = false;
        [_statusZIP setStringValue:@"Fail :("];
        _statusZIP.textColor = [NSColor systemRedColor];
    }
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(doneZIP) userInfo:nil repeats:NO];
}

- (void)doneZIP {
    _statusZIP.hidden = true;
}

- (IBAction)clickChooseFile:(id)sender {
    // Create the File Open Dialog class.
    NSOpenPanel *openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles:NO];
    [openDlg setAllowsMultipleSelection:NO];
    [openDlg setCanChooseDirectories:YES];
    [openDlg setCanCreateDirectories:YES];
    [openDlg setCanSelectHiddenExtension:YES];
    [openDlg setTitle:@"Chọn file mong muốn"];
    if ([openDlg runModal] == NSModalResponseOK) {
        // Get an array containing the full filenames of all files and directories selected.
        NSArray *urls = [openDlg URLs];
        NSURL *selection = urls[0];
        inputPATH = [[selection path] stringByResolvingSymlinksInPath];

        // Loop through all the files and process them.
        for (int i = 0; i < [urls count]; i++) {
            NSString *url = urls[(NSUInteger) i];
            NSLog(@"Url: %@", url);
        }
        [_selectedPath setStringValue:[Tools append:@"Input: ",inputPATH, nil]];
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (NSString *)getFileName {
    NSDateFormatter *timeNow = [[NSDateFormatter alloc] init];
    [timeNow setDateFormat:@"yyyy_MM_dd hh_mm_ss"];
    NSLog(@"%@", [timeNow stringFromDate:[NSDate date]]);
    return [Tools append:@"use", [timeNow stringFromDate:[NSDate date]], @".zip", nil];
}

- (void)openZippiedPATH {
    NSArray *fileURLs = @[[NSURL fileURLWithPath:outputPath]];
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:fileURLs];
}
@end
