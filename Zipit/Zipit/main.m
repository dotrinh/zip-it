//
//  main.m
//  Zipit
//
//  Created by Do Trinh on 2020/02/05.
//  Copyright © 2020 Do Trinh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
